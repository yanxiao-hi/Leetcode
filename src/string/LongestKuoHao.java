package string;


import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.ArrayDeque;
import java.util.Deque;

public class LongestKuoHao {
    public static void main(String[] args) {
        LongestKuoHao longestKuoHao = new LongestKuoHao();
        int len = longestKuoHao.longeestValidKuoHao(")()()");
        System.out.println(len);
    }

    public int longeestValidKuoHao(String s) {
        if (s == null || s.length() == 0) return 0;
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(-1);
        int res = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                stack.push(i);
            } else {
                stack.pop();
                if (stack.isEmpty()) {
                    stack.push(i);//栈空，入栈当参照物
                } else {
                    res = Math.max(res, i - stack.peek());
                }
            }
        }
        return res;

    }
}
