package list;

import java.util.List;

/**
 * @Author yanxiao
 * @Date 2020-12-10 17:02
 **/
/*class ListNode{
    int val;
    ListNode next;
    ListNode(int x){
        val = x;
        next = null;
    }
}*/

public class ReverseList {
    public static void main(String[] args) {
        ReverseList reverseList = new ReverseList();
    }

    public ListNode reverseList(ListNode head) {
        //申请节点，pre和 cur，pre指向null
        ListNode pre = null;
        ListNode cur = head;
        ListNode tmp = null;
        while(cur!=null){
            tmp = cur.next;
            cur.next = pre;
            pre = cur;
            cur = tmp;
        }
        return pre;

    }
}

